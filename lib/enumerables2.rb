require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |sum, num| sum + num }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
    long_strings.all? { |el| el.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  # string.each_char.reduce([]) do |arr, ch|
  #   string.chars.count(ch) > 1 && !arr.include?(ch) ? arr << ch : arr
  # end
  new_str = string.split.join('').chars.uniq
  new_arr = []
  new_str.each do |ch|
    new_arr << ch if string.count(ch) >= 2
  end
  new_arr
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted_string = string.split.sort_by { |word| word.length }
  [sorted_string[-1], sorted_string[-2]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  a_to_z = ('a'..'z').to_a
  a_to_z.reject { |ch| string.chars.include?(ch) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  years = (first_yr..last_yr)
  years.reduce([]) { |arr, year| not_repeat_year?(year) ? arr << year : arr}
end

def not_repeat_year?(year)
  year = year.to_s.chars
  bool = true
  year.each { |el| bool = false if year.count(el) > 1 }
  bool
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.reduce([]) do |arr, song|
    no_repeats?(song, songs) && !arr.include?(song) ? arr << song : arr
  end
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, i|
    return false if song_name == song && song == songs[i + 1]
  end
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string = string.delete('.?!,"')
  position = string.split.length
  closest_c_word = ''
  string.split(' ').each do |word|
    if c_distance(word) != nil && c_distance(word) < position
      closest_c_word = word
      position = c_distance(word)
    end
  end
  closest_c_word
end

def c_distance(word)
  word.split("").reverse.each_with_index do |ch, i|
    return i if ch.downcase == 'c'
  end
  nil
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  new_arr = []
  pair = []
  arr.each_with_index do |num, i|
    if num == arr[i + 1] && pair.empty?
      pair << i
    elsif num == arr[i - 1] && num != arr[i + 1]
      pair << i
      new_arr << pair
      pair = []
    end
  end
  new_arr
end
